# LSTM Stock Predictor

Due to the volatility of cryptocurrency speculation, investors will often try to incorporate sentiment from social media and news articles to help guide their trading strategies. One such indicator is the Crypto Fear and Greed Index (FNG) which attempts to use a variety of data sources to produce a daily FNG value for cryptocurrency. I have built and evaluated deep learning models using both the FNG values and simple closing prices to determine if the FNG indicator provides a better signal for cryptocurrencies than the normal closing price data.
In this project I have used deep learning recurrent neural networks to model bitcoin closing prices. One model will use the FNG indicators to predict the closing price while the second model  uses a window of closing prices to predict the nth closing price.

# Steps
1) Prepare the data for training and testing
2) Build and train custom LSTM RNNs
3) Evaluate the performance of each model

# Instructions

## Prepare the data for training and testing

code contains a function to create the window of time for the data in each dataset.

For the Fear and Greed model, I have used the FNG values to try and predict the closing price. A function is provided in the notebook to help with this.

For the closing price model, I have used previous closing prices to try and predict the next closing price. A function is provided in the notebook to help with this.

Each model uses 70% of the data for training and 30% of the data for testing.

A MinMaxScaler applied to the X and y values to scale the data for the model.

Finally, reshaped the X_train and X_test values to fit the model's requirement of samples, time steps, and features.

## Build and train custom LSTM RNNs

In each Jupyter Notebook, I have created the same custom LSTM RNN architecture. In one notebook, I fit the data using the FNG values. In the second notebook, I fit the data using only closing prices.

## Evaluate the performance of each model

Finally, use the testing data to evaluate each model and compare the performance.

Use the above to answer the following:

> Which model has a lower loss?
>
> Which model tracks the actual values better over time?
>
> Which window size works best for the model?

- - -

